#!/bin/bash
# Name:Said Kharmich Fourkati
# Data: 08/06/2021
# Usage: bash pregunta1.sh
# File Location: /opt
# Description:

DATE=$(date +"%Y%m%d")

touch /tmp said_kharmich_${DATE}.txt

if [ $? -eq 0 ];
then
    echo "S'ha creat el fitxer"
else
    echo "No s'ha pogut crear el fitxer"
    exit 1
fi

echo said_kharmich > /tmp said_kharmich${DATE}.txt

if [ $? -eq 0 ];
then
    echo "S'ha afegit el said kharmich"
else
    echo "No s'ha pogut afegit el said kharmich"
    exit 1
fi

echo $(date "+%Y-&m-&d-&H:&M:%s") > /tmp said_kharmich${DATE}.txt

if [ $? -eq 0 ];
then
    echo "S'ha afegit la data"
else
    echo "No s'ha pogut afegit la data"
    exit 1
fi
